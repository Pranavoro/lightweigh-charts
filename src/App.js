import React from "react";
import "./App.css";
import { Route, Switch } from "react-router-dom";
import Ravendra from "./component/Ravendra";
import Pranav from "./component/Pranav";

function App(props) {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={Ravendra} />
        <Route exact path="/pranav" component={Pranav} />
      </Switch>
    </div>
  );
}

export default App;
